import sys
import time

import numpy as np
import pyximport

pyximport.install()

import algebra_python
import algebra_python_brute_force
import algebra_python_vectorize
import algebra_cython_optimize_0
import algebra_cython_optimize_1
import algebra_cython_optimize_2
import algebra_cython_optimize_3
import algebra_cython_optimize_4
import algebra_cython_optimize_5

size = 1000
try:
    size = int(sys.argv[1])
except:
    pass

a = np.random.rand(size, size).astype(np.float32)
b = np.random.rand(size, size).astype(np.float32)

index = 0

index += 1
print('#{0:02d}. Shape: {1}x{1}'.format(index, size))
print()

index += 1
s = time.time()
x = algebra_python.conv(a, b)
print('#{0:02d}. algebra_python'.format(index))
print('  * elapsed time: {}'.format(time.time() - s))
print()

index += 1
s = time.time()
y = algebra_python_brute_force.conv(a, b)
print('#{0:02d}. algebra_python_brute_force'.format(index))
print('  * elapsed time: {}'.format(time.time() - s))
print('  * isclose: {}'.format(np.isclose(x, y).all()))
print()
del y

index += 1
s = time.time()
y = algebra_python_vectorize.conv(a, b)
print('#{0:02d}. algebra_python_vectorize'.format(index))
print('  * elapsed time: {}'.format(time.time() - s))
print('  * isclose: {}'.format(np.isclose(x, y).all()))
print()
del y

index += 1
s = time.time()
y = algebra_cython_optimize_0.conv(a, b)
print('#{0:02d}. algebra_cython_optimize_0'.format(index))
print('  * elapsed time: {}'.format(time.time() - s))
print('  * isclose: {}'.format(np.isclose(x, y).all()))
print()
del y

index += 1
s = time.time()
y = algebra_cython_optimize_1.conv(a, b)
print('#{0:02d}. algebra_cython_optimize_1'.format(index))
print('  * elapsed time: {}'.format(time.time() - s))
print('  * isclose: {}'.format(np.isclose(x, y).all()))
print()
del y

index += 1
s = time.time()
y = algebra_cython_optimize_2.conv(a, b)
print('#{0:02d}. algebra_cython_optimize_2'.format(index))
print('  * elapsed time: {}'.format(time.time() - s))
print('  * isclose: {}'.format(np.isclose(x, y).all()))
print()
del y

index += 1
s = time.time()
y = algebra_cython_optimize_3.conv(a, b)
print('#{0:02d}. algebra_cython_optimize_3'.format(index))
print('  * elapsed time: {}'.format(time.time() - s))
print('  * isclose: {}'.format(np.isclose(x, y).all()))
print()
del y

index += 1
s = time.time()
y = algebra_cython_optimize_4.conv(a, b)
print('#{0:02d}. algebra_cython_optimize_4'.format(index))
print('  * elapsed time: {}'.format(time.time() - s))
print('  * isclose: {}'.format(np.isclose(x, y).all()))
print()
del y

index += 1
s = time.time()
y = algebra_cython_optimize_5.conv(a, b)
print('#{0:02d}. algebra_cython_optimize_5'.format(index))
print('  * elapsed time: {}'.format(time.time() - s))
print('  * isclose: {}'.format(np.isclose(x, y).all()))
print()
del y
