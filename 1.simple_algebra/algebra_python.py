import numpy as np


def conv(a, b):
    assert (a.shape == b.shape)
    return np.sin(a)*np.cos(a)*np.tan(a)*np.sqrt(a)*np.exp(a)*np.sin(b)*np.cos(b)*np.tan(b)*np.sqrt(b)*np.exp(b)
