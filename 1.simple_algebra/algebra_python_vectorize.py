import numpy as np
import math

def conv(A, B):
    assert (A.shape == B.shape)
    C = np.empty_like(A)

    def func(a, b):
        return math.sin(a) * math.cos(a) * math.tan(a) * math.sqrt(a) * math.exp(a) * math.sin(b) * math.cos(b) * math.tan(b) * math.sqrt(
            b) * math.exp(b)

    vfunc = np.vectorize(func)

    return vfunc(A, B)
