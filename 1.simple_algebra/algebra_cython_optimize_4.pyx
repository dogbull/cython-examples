# ref:
#   http://cython.readthedocs.io/en/latest/src/tutorial/numpy.html

import numpy as np

from libc.math cimport sin, cos, tan, sqrt, exp

cimport numpy as np
cimport cython

from cython.parallel import prange


@cython.profile(False)
@cython.boundscheck(False)
@cython.wraparound(False)
def conv(float[:,:] ra, float[:,:] rb):
    cdef int x
    cdef int y
    cdef int i
    cdef float a
    cdef float b
    cdef int w = ra.shape[1]
    cdef int h = ra.shape[0]
    cdef int size = w * h
    cdef float[:,:] rc = np.empty_like(ra)

    with nogil:
        for i in prange(size):
            x = <int>(i%w)
            y = <int>(i/h)
            a = ra[y, x]
            b = rb[y, x]
            rc[y, x] = sin(a)*cos(a)*tan(a)*sqrt(a)*exp(a)*sin(b)*cos(b)*tan(b)*sqrt(b)*exp(b)
    return rc
