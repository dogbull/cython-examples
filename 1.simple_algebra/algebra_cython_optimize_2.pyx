# ref:
#   http://cython.readthedocs.io/en/latest/src/tutorial/numpy.html

import numpy as np

from libc.math cimport sin, cos, tan, sqrt, exp

cimport numpy as np

cimport cython

@cython.profile(False)
@cython.boundscheck(False)
@cython.wraparound(False)
def conv(np.ndarray[np.float32_t, ndim=2] ra, np.ndarray[np.float32_t, ndim=2] rb):
    cdef float a
    cdef float b
    cdef np.ndarray[np.float32_t, ndim=2] rc = np.zeros((ra.shape[0], ra.shape[1]), dtype=np.float32)
    for y in range(ra.shape[0]):
        for x in range(ra.shape[1]):
            a = ra[y, x]
            b = rb[y, x]
            rc[y, x] = sin(a)*cos(a)*tan(a)*sqrt(a)*exp(a)*sin(b)*cos(b)*tan(b)*sqrt(b)*exp(b)
    return rc
