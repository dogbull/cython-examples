import numpy as np


def conv(A, B):
    assert (A.shape == B.shape)
    C = np.empty_like(A)

    for y in range(A.shape[0]):
        for x in range(A.shape[1]):
            a = A[y, x]
            b = B[y, x]

            C[y,x] = np.sin(a)*np.cos(a)*np.tan(a)*np.sqrt(a)*np.exp(a)*np.sin(b)*np.cos(b)*np.tan(b)*np.sqrt(b)*np.exp(b)
    return C
